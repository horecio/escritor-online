<?php
namespace Projeto\Controller;

use Base\Controller\AbstractController;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Storage\Session;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractController
{
    function __construct()
    {
        $this->controller = 'index';
        $this->action = 'index';
        $this->entity = 'Projeto\Entity\Projeto';
        $this->form = 'Projeto\Form\Projeto';
        $this->route = 'projeto/default';
        $this->service = 'Projeto\Service\Projeto';
    }

    public function addAction()
    {
        $auth = new AuthenticationService();
        $auth->setStorage(new Session('User'));

        $form = $this->getServiceLocator()
            ->get($this->form);
        $request = $this->getRequest();

        if ($request->isPost()) {

            if (isset($request->getPost()['temas'])) {
                //Usar Strategy - ALTERAçÃO
                //Get Referência Entity\Tema
                $id = $request->getPost()['temas'];
                $entityTema = $this->getEm()
                    ->getReference('Tema\Entity\Tema', $id);

                //Get Referência Entity\UsuarioLogin
                if ($auth->hasIdentity()) {
                    //Usuário Logado
                    $user = $auth->getIdentity()['user'];

                    //Get Entity do usuário logado
                    $entityUser = $this->getEm()
                        ->getReference('User\Entity\UsuarioLogin', $user->getId());

                    //Set Referências na Entity\Projeto
                    $entity = $this->getServiceLocator()
                        ->get($this->entity);
                    $entity->setTema($entityTema)
                        ->setUsuarioLogin($entityUser);
                    $form->bind($entity);
                }
            }
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $service = $this->getServiceLocator()->get($this->service);

                try {
                    $service->insert($form->getData());

                    $this->flashMessenger()->addSuccessMessage("Cadastrado com sucesso");
                    return $this->redirect()->toRoute($this->route);
                } catch (\Exception $e) {
                    $this->flashMessenger()->addErrorMessage("Ops! Algum problema ocorreu ao inserir o usuário" . $e->getMessage());
                    return $this->redirect()
                        ->toRoute($this->route);
                }
            }
        }
        return new ViewModel(array(
            'form' => $form,
            'controller' => $this->controller,
            'route' => $this->route
        ));
    }
}