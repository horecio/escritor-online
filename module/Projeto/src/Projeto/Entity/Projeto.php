<?php
namespace Projeto\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Projeto
 *
 * @ORM\Table(name="eo_t_projetos", uniqueConstraints={@ORM\UniqueConstraint(name="id_UNIQUE", columns={"id"})}, indexes={@ORM\Index(name="fk_eo_t_projetos_eo_t_tema1_idx", columns={"eo_t_tema_id"}), @ORM\Index(name="fk_eo_t_projetos_eo_t_login1_idx", columns={"eo_t_login_id"})})
 * @ORM\Entity(repositoryClass="Projeto\Entity\Repository\Projeto")
 */
class Projeto
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="assunto", type="string", length=255, nullable=false)
     */
    private $assunto;

    /**
     * @var string
     *
     * @ORM\Column(name="descricao", type="text", nullable=false)
     */
    private $descricao;

    /**
     * @var boolean
     *
     * @ORM\Column(name="status", type="boolean", nullable=false)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_fim_proj", type="datetime", nullable=false)
     */
    private $dtFimProj;

    /**
     * @var \User\Entity\UsuarioLogin
     *
     * @ORM\ManyToOne(targetEntity="User\Entity\UsuarioLogin")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="eo_t_login_id", referencedColumnName="id")
     * })
     */
    private $usuarioLogin;

    /**
     * @var \Tema\Entity\Tema
     *
     * @ORM\ManyToOne(targetEntity="Tema\Entity\Tema")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="eo_t_tema_id", referencedColumnName="id")
     * })
     */
    private $tema;

    function __construct()
    {
        $this->setCreatedAt();
        $this->setStatus(true);
    }

    /**
     * @return \DateTime
     */
    public function getTime()
    {
        return new \DateTime('now', new \DateTimeZone('America/Araguaina'));;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set assunto
     *
     * @param string $assunto
     * @return Projeto
     */
    public function setAssunto($assunto)
    {
        $this->assunto = $assunto;

        return $this;
    }

    /**
     * Get assunto
     *
     * @return string 
     */
    public function getAssunto()
    {
        return $this->assunto;
    }

    /**
     * Set descricao
     *
     * @param string $descricao
     * @return Projeto
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;

        return $this;
    }

    /**
     * Get descricao
     *
     * @return string 
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * Set status
     *
     * @param boolean $status
     * @return Projeto
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set createdAt
     *
     * @internal param \DateTime $createdAt
     * @return Projeto
     */
    public function setCreatedAt()
    {
        $this->createdAt = $this->getTime();

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set dtFimProj
     *
     * @param \DateTime $dtFimProj
     * @return Projeto
     */
    public function setDtFimProj(\DateTime $dtFimProj)
    {
        $this->dtFimProj = $dtFimProj;

        return $this;
    }

    /**
     * Get dtFimProj
     *
     * @return \DateTime 
     */
    public function getDtFimProj()
    {
        return $this->dtFimProj;
    }

    /**
     * Set UsuarioLogin
     *
     * @param \User\Entity\UsuarioLogin $usuarioLogin
     * @return Projeto
     */
    public function setUsuarioLogin(\User\Entity\UsuarioLogin $usuarioLogin = null)
    {
        $this->usuarioLogin = $usuarioLogin;

        return $this;
    }

    /**
     * Get usuarioLogin
     *
     * @return \User\Entity\UsuarioLogin
     */
    public function getUsuarioLogin()
    {
        return $this->usuarioLogin;
    }

    /**
     * Set Tema
     *
     * @param \Tema\Entity\Tema $tema
     * @return Projeto
     */
    public function setTema(\Tema\Entity\Tema $tema = null)
    {
        $this->tema = $tema;

        return $this;
    }

    /**
     * Get Tema
     *
     * @return \Tema\Entity\Tema
     */
    public function getTema()
    {
        return $this->tema;
    }
}
