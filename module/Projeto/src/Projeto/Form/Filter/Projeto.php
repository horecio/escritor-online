<?php
namespace Projeto\Form\Filter;

use Zend\InputFilter\InputFilter;

class Projeto extends InputFilter
{
    function __construct()
    {
        $this->add(array(
            'name' => 'temas',
            'required' => true,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
        ));
        $date = new \DateTime('now');
        $this->add(array(
            'name' => 'dtFimProj',
            'required' => true,
//            'filters' => array(
//                array('name' => 'StripTags'),
//                array('name' => 'StringTrim'),
//            ),
            'validators' => array(
                array(
                    'name' => 'GreaterThan',
                    'options' => array(
                        'min' => $date->format('d/m/Y'),
                        'inclusive' => true,
                    )
                )
            )
        ));
        $this->add(array(
            'name' => 'assunto',
            'required' => true,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
            'validators' => array()
        ));
        $this->add(array(
            'name' => 'descricao',
            'required' => true,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
                array(
                    'name' => 'StringLength',
                    'options' => array(
                        'encoding' => 'UTF-8',
                        'min' => 5,
                        'max' => 1000,
                        'messages' => array(
                            'stringLengthTooShort' => 'Sua descrição está muito pequena. Mínimo: 5 caracteres.',
                            'stringLengthTooLong' => 'Sua descrição está muito grande. Máximo 1000(mil) caracteres.'
                        )
                    ),
                ),
//                array(
//                    'name' => 'Regex',
//                    'options' => array(
//                        'pattern' => '/^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/',
//                        'messages' => array(
//                            'regexNotMatch' => 'Seu email está estranho. Tente novamente'
//                        )
//                    )
//                )

            )
        ));
    }
}
