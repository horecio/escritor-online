<?php
namespace Projeto\Form;

use Base\Hydrator\Strategy\DateTimeStrategy;
use Base\Hydrator\Strategy\TemaEntityStrategy;
use Doctrine\ORM\EntityManager;
use Zend\Form\Form;
use Zend\Stdlib\Hydrator\ClassMethods;
use Projeto\Entity\Projeto as ProjetoEntity;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Storage\Session;

class Projeto extends Form{
    function __construct($name = null, EntityManager $em = null)
    {
        parent::__construct($name);
        //Adiciona strategy no hydrate para data-fim ser convertido em \DateTime
        $hydrator = new ClassMethods(false);
        $hydrator->addStrategy('dtFimProj', new DateTimeStrategy());

        $this->setHydrator($hydrator)
            ->setObject(new ProjetoEntity())
            ->setInputFilter(new Filter\Projeto())
            ->setAttribute('method', 'post');

        $auth = new AuthenticationService();
        $auth->setStorage(new Session('User'));
        if ($auth->hasIdentity()) {
        $user = $auth->getIdentity()['user'];
        $this->add(array(
            'name' => 'temas',
            'type' => 'DoctrineModule\Form\Element\ObjectRadio',
            'options' => array(
                'label' => 'Escolha de 1 tema',
                'object_manager' => $em,
                'target_class' => 'Tema\Entity\Tema',
                'property' => 'nome',
                'is_method' => true,
                'find_method' => array(
                    'name'   => 'temasByUser',
                    'params' => array(
                        'criteria' => array('id' => $user->getId())
                    ),
                ),
            ),
        ));
        } else {
            //remover
            echo "Nenhuma Usuário Logado";
        }
        $date = new \DateTime('now');
        $this->add(array(
                'name' => 'dtFimProj',
                'type' => 'Text',
                'options' => array(
                    'label' => 'Data Fim do Projeto',
                    'format' => 'd/m/Y',
                ),
                'attributes' => array(
                    'min' => $date->format('d/m/Y'),
                    'class' => 'form-control',
                )
            )
        );
        $this->add(array(
                'name' => 'assunto',
                'type' => 'Text',
                'options' => array(),
                'attributes' => array(
                    'placeholder' => 'ASSUNTO',
                    'class' => 'form-control',
                )
            )
        );
        $this->add(array(
                'name' => 'descricao',
                'type' => 'TextArea',
                'options' => array(),
                'attributes' => array(
                    'placeholder' => 'DESCRIÇÃO',
                    'class' => 'form-control',
                )
            )
        );
        $this->add(array(
            'name' => 'csrf',
            'type' => 'Zend\Form\Element\Csrf',
        ));
        $this->add(array(
            'name' => 'Submit',
            'type'=>'Zend\Form\Element\Submit',
            'attributes' => array(
                'value'=>'Salvar',
                'class' => 'btn btn-success'
            )
        ));
    }
}