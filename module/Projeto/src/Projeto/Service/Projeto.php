<?php
namespace Projeto\Service;

use Base\Service\AbstractService;
use Doctrine\ORM\EntityManager;

class Projeto extends AbstractService
{
    function __construct(EntityManager $em)
    {
        parent::__construct($em);
        $this->entity = 'Projeto\Entity\Projeto';
    }
}