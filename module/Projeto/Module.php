<?php
namespace Projeto;

use Zend\Mvc\I18n\Translator;

class Module
{
    public function onBootstrap()
    {
        /**
         * Alterar local para não carregar em todos os momentos inclusive em ágina que não tem form
         * Tradução de Mensagens Validators
         */
        //Cria Translator
        $translator = new Translator(new \Zend\I18n\Translator\Translator());

        //Adiciona o arquivo de tradução
        $translator->addTranslationFile(
            'phpArray',
            __DIR__ . '/../../vendor/zendframework/zendframework/resources/languages/pt_BR/Zend_Validate.php',
            'default',
            'pt_BR'
        );
        //Define o tradutor padrão dos validadores
        \Zend\Validator\AbstractValidator::setDefaultTranslator($translator);
    }
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'Projeto\Service\Projeto' => function($serviceManager) {
                        return new Service\Projeto(
                            $serviceManager->get('Doctrine\ORM\EntityManager')
                        );
                    },
                'Projeto\Form\Projeto' => function($serviceManager){
                        return new Form\Projeto(
                            'projeto',
                            $serviceManager->get('Doctrine\ORM\EntityManager')
                        );
                    },
            ),
            'invokables' => array(
                'Projeto\Entity\Projeto' => 'Projeto\Entity\Projeto',
            )
        );
    }
}
