<?php
namespace Tema\Service;

use Base\Service\AbstractService;
use Doctrine\ORM\EntityManager;

class Tema extends AbstractService
{
    function __construct(EntityManager $em)
    {
        parent::__construct($em);
        $this->entity = 'Tema\Entity\Tema';
    }
}