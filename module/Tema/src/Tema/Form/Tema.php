<?php
namespace Tema\Form;

use Zend\Form\Form;
use Zend\Stdlib\Hydrator\ClassMethods;
use Tema\Entity\Tema as TemaEntity;

class Tema extends Form{
    public function __construct($name = 'tema')
    {
        parent::__construct($name);
        $this->setAttribute('method', 'post')
            ->setHydrator(new ClassMethods(false))
            ->setObject(new TemaEntity());

        $this->add(array(
                'name' => 'nome',
                'type' => 'Text',
                'options' => array(),
                'attributes' => array(
                    'placeholder' => 'Nome',
                    'class' => 'form-control',
                )
            )
        );
        $this->add(array(
                'name' => 'descricao',
                'type' => 'TextArea',
                'options' => array(),
                'attributes' => array(
                    'placeholder' => 'Nome',
                    'class' => 'form-control',
                )
            )
        );
        $this->add(array(
            'name' => 'csrf',
            'type' => 'Zend\Form\Element\Csrf',
        ));
        $this->add(array(
            'name' => 'Submit',
            'type'=>'Zend\Form\Element\Submit',
            'attributes' => array(
                'value'=>'Salvar',
                'class' => 'btn-success'
            )
        ));
    }
}