<?php
namespace Tema\Controller;

use Base\Controller\AbstractController;

class IndexController extends AbstractController
{
    function __construct()
    {
        $this->controller = 'index';
        $this->action = 'index';
        $this->entity = 'Tema\Entity\Tema';
        $this->form = 'Tema\Form\Tema';
        $this->route = 'tema/default';
        $this->service = 'Tema\Service\Tema';
    }
}