<?php
namespace Tema\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class Tema extends EntityRepository
{
//    public function fetchPairs(){
//        $entities = $this->findAll();
//        $array = array();
//        foreach ($entities as $entity){
//            $array[$entity->getId()] = $entity->getNome();
//        }
//
//        return $array;
//    }
    /**
     * @param array $criteria
     * @return array
     */
    public function temasByUser(array $criteria)
    {
        //Retorna obj de temas de usuário
        return $this->createQueryBuilder('t')
            ->select('t')
            ->innerJoin("t.usuarioLogin", "u", "WITH", "u=:userid")
            ->setParameter("userid", $criteria['id'])
            ->getQuery()->getResult();
    }
} 