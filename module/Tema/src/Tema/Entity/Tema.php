<?php
namespace Tema\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * Tema
 *
 * @ORM\Table(name="eo_t_tema", uniqueConstraints={@ORM\UniqueConstraint(name="id_UNIQUE", columns={"id"})})
 * @ORM\Entity(repositoryClass="Tema\Entity\Repository\Tema")
 */
class Tema
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=255, nullable=false)
     */
    private $nome;

    /**
     * @var string
     *
     * @ORM\Column(name="descricao", type="text", nullable=false)
     */
    private $descricao;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="User\Entity\UsuarioLogin", mappedBy="tema", inversedBy="tema")
     */
    private $usuarioLogin;

    function __construct()
    {
        $this->usuarioLogin = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Set nome
     *
     * @param string $nome
     * @return Tema
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set descricao
     *
     * @param string $descricao
     * @return Tema
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;

        return $this;
    }

    /**
     * Get descricao
     *
     * @return string
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * Add usuarioLogin
     *
     * @param \User\Entity\UsuarioLogin $usuarioLogin
     * @return Tema
     */
    public function addUsuarioLogin(\User\Entity\UsuarioLogin $usuarioLogin)
    {
        $this->usuarioLogin[] = $usuarioLogin;

        return $this;
    }

    /**
     * Remove usuarioLogin
     *
     * @param \User\Entity\UsuarioLogin $usuarioLogin
     */
    public function removeUsuarioLogin(\User\Entity\UsuarioLogin $usuarioLogin)
    {
        $this->usuarioLogin->removeElement($usuarioLogin);
    }

    /**
     * Get usuarioLogin
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsuarioLogin()
    {
        return $this->usuarioLogin;
    }
}
