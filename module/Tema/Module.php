<?php
namespace Tema;

class Module
{
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'Tema\Service\Tema' => function($serviceManager) {
                        return new Service\Tema(
                            $serviceManager->get('Doctrine\ORM\EntityManager')
                        );
                    }
            ),
            'invokables' => array(
                'Tema\Entity\Tema' => 'Tema\Entity\Tema',
                'Tema\Form\Tema' => 'Tema\Form\Tema'
            )
        );
    }
}
