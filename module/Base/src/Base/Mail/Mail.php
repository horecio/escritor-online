<?php
namespace Base\Mail;


use Zend\Mail\Message as MailMessage;
use Zend\Mail\Transport\Smtp;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part;
use Zend\View\Model\ViewModel;

class Mail
{
    protected $transport;
    protected $view;
    protected $body;
    protected $message;
    protected $subject; //assunto
    protected $to;
    protected $data; //variáveis que irão para view
    protected $page; //página que será reenderizada

    function __construct(Smtp $transport, $view, $page)
    {
        $this->transport = $transport;
        $this->view = $view;
        $this->page = $page;
    }

    /**
     * @param mixed $subject
     * @return Mail
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
        return $this;
    }

    /**
     * @param mixed $to
     * @return Mail
     */
    public function setTo($to)
    {
        $this->to = $to;
        return $this;
    }

    /**
     * @param mixed $data
     * @return Mail
     */
    public function setData(array $data)
    {
        $this->data = $data;
        return $this;
    }

    /**
     * Reenderizaão da View com dados de $data, page e a view
     */
    public function renderView($page, array $data)
    {
        $model = new ViewModel();
        $model->setTemplate("mailer/{$page}.phtml") //template utilizado para reenderizar
            ->setOption('has_parent', true) //
            ->setVariables($data); //Variáveis utulizadas para reenderizar

        return $this->view
            ->render($model);
    }

    //Prepara mensagem para ser enviada
    public function prepare()
    {
        /*
         * Aqui é a mensagem
         * HTML reenderizado com Mime\Part
         */
        $html = new Part($this->renderView($this->page, $this->data));
        $html->type = "text/html"; //tipo da mensagem

        /*
         * Corpo da mensagem
         */
        $body = new MimeMessage();
        $body->setParts(array($html)); //pode colocar vários Mime\Part
        $this->body = $body;

        /*
         * Pega configuração de envio de email no global.php
         */
        $config = $this->transport
            ->getOptions()
            ->toArray();

        //Seta a mensagem
        $this->message = new MailMessage();
        $this->message->addFrom($config['connection_config']['from'])
            ->addTo($this->to)
            ->setSubject($this->subject)
            ->setBody($this->body);

        return $this;
    }

    /*
     * Envia Mensagem
     */
    public function send()
    {
        $this->transport->send($this->message);
    }
}