<?php
namespace Base\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

abstract class AbstractController extends AbstractActionController
{
    /**
     * @var EntityManager
     */
    protected $em;
    protected $entity;
    protected $service;
    protected $form;
    protected $route;
    protected $controller;
    protected $action;

    public function indexAction()
    {
        try{
        $list = $this->getEm()
            ->getRepository($this->entity)
            ->findAll();
        }catch (\Exception $e){
            $this->flashMessenger()->addErrorMessage("Ops! Algum problema ocorreu ao Listar" . $e->getMessage());
            return $this->redirect()
                ->toRoute($this->route);
        }
        return new ViewModel(array(
            'list' => $list,
            'controller' => $this->controller,
            'route' => $this->route
        ));
    }

    public function addAction()
    {
        $form = $this->getServiceLocator()
            ->get($this->form);
        $request = $this->getRequest();

        if ($request->isPost()) {
            $entity = $this->getServiceLocator()
                ->get($this->entity);
            $form->bind($entity);
            $form->setData($request->getPost());

            if ($form->isValid()) {

                $service = $this->getServiceLocator()->get($this->service);
                try {
                    $service->insert($form->getData());

                    $this->flashMessenger()->addSuccessMessage("Cadastrado com sucesso");
                    return $this->redirect()->toRoute($this->route);
                } catch (\Exception $e) {
                    $this->flashMessenger()->addErrorMessage("Ops! Algum problema ocorreu ao inserir o usuário");
                    return $this->redirect()
                        ->toRoute($this->route);
                }
            }
        }
        return new ViewModel(array(
            'form' => $form,
            'controller' => $this->controller,
            'route' => $this->route
        ));
    }

    public function editAction()
    {
        if (!$id = $this->params()->fromRoute('id', 0)) {
            $this->flashMessenger()->addErrorMessage("Ops! Precisa do ID");
            return $this->redirect()
                ->toRoute($this->route);
        }

        $form = $this->getServiceLocator()
            ->get($this->form);
        $request = $this->getRequest();

        try {
            $entity = $this->getEm()
                ->getRepository($this->entity)
                ->find($id);
            $form->bind($entity);
        } catch (\Exception $e) {
            $this->flashMessenger()->addErrorMessage("Parece que esse ID não existe.");
            return $this->redirect()
                ->toRoute($this->route);
        }

        if ($request->isPost()) {
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $service = $this->getServiceLocator()
                    ->get($this->service);
                $service->update($form->getData());

                $this->flashMessenger()->addSuccessMessage('Atualizado com sucesso');
                return $this->redirect()
                    ->toRoute($this->route);
            }
        }
        return new ViewModel(array(
            'form' => $form,
            'id' => $id,
            'controller' => $this->controller,
            'route' => $this->route
        ));
    }

    public function deleteAction()
    {
        if (!$id = (int)$this->params()->fromRoute('id', 0)) {
            $this->flashMessenger()->addErrorMessage("Ops! Precisa do ID");
            return $this->redirect()
                ->toRoute($this->route);
        }

        $service = $this->getServiceLocator()
            ->get($this->service);

        try {
            $service->delete($id);
            $this->flashMessenger()->addSuccessMessage('Deletado com sucesso');
            return $this->redirect()
                ->toRoute($this->route, array('controller' => $this->controller));
        } catch (\Exception $e) {
            $this->flashMessenger()->addErrorMessage("Parece que esse ID não existe.");
            return $this->redirect()
                ->toRoute($this->route, array('controller' => $this->controller));
        }
    }

    public function disableAction()
    {
        if (!$id = (int)$this->params()->fromRoute('id', 0)) {
            $this->flashMessenger()->addErrorMessage("Parece que esse ID não existe.");
            return $this->redirect()
                ->toRoute($this->route);
        }

        $service = $this->getServiceLocator()
            ->get($this->service);

        try {
            $service->disable($id);
            $this->flashMessenger()->addSuccessMessage('Desabilitado com sucesso');
            return $this->redirect()
                ->toRoute($this->route, array('controller' => $this->controller));
        } catch (\Exception $e) {
            $this->flashMessenger()->addErrorMessage("Parece que esse ID não existe.");
            return $this->redirect()
                ->toRoute($this->route, array('controller' => $this->controller));
        }
    }

    public function detailAction()
    {
        if (!$id = (int)$this->params()->fromRoute('id', 0)) {
            $this->flashMessenger()->addErrorMessage("Ops! Precisa do ID");
            return $this->redirect()
                ->toRoute($this->route);
        }

        try {
        $entity = $this->getEm()
            ->getRepository($this->entity)
            ->find($id);
        } catch (\Exception $e) {
            $this->flashMessenger()->addErrorMessage("Parece que esse ID não existe.");
            return $this->redirect()
                ->toRoute($this->route, array('controller' => $this->controller));
        }

        return new ViewModel(array(
            'controller' => $this->controller,
            'detail' => $entity,
            'route' => $this->route
        ));
    }

    /**
     *
     * @return EntityManager
     */
    protected function getEm()
    {
        if (null === $this->em)
            $this->em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');

        return $this->em;
    }
}