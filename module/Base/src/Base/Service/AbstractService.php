<?php
/**
 * Created by PhpStorm.
 * User: AMZ Dev
 * Date: 15/08/14
 * Time: 17:00
 */

namespace Base\Service;

use Doctrine\ORM\EntityManager;

abstract class AbstractService
{
    private $em;
    protected $entity;

    function __construct(EntityManager $entityManager = null)
    {
        $this->em = $entityManager;
    }

    public function insert($entity)
    {
        $this->getEm()->persist($entity);
        $this->getEm()->flush();

        return $entity;
    }

    public function update($entity)
    {
        $this->getEm()->persist($entity);
        $this->getEm()->flush();

        return $entity;
    }

    public function delete($id)
    {
        $id = (int) $id;
        if (!is_int($id)) {
            $messageException = 'Id deve ser um número inteiro';
            throw new \InvalidArgumentException($messageException);
        }

        if ($id <= 0) {
            $messageException = 'Id deve ser maior do que zero';
            throw new \OutOfBoundsException($messageException);
        }

        $entity = $this->getEm()->getReference($this->entity, $id);

        $this->getEm()->remove($entity);
        $this->getEm()->flush();

        return $id;
    }

    /*
     * Altera Status para false
     */
    public function disable($id)
    {
        if (!is_int($id)) {
            $messageException = 'Id deve ser um número inteiro';
            throw new \InvalidArgumentException($messageException);
        }

        if ($id <= 0) {
            $messageException = 'Id deve ser maior do que zero';
            throw new \OutOfBoundsException($messageException);
        }

        $entity = $this->getEm()
            ->getReference($this->entity, $id);
        $entity->setStatus(false);

        $this->getEm()->persist($entity);
        $this->getEm()->flush();

        return $entity;
    }

    public function getEm()
    {
        return $this->em;
    }

} 