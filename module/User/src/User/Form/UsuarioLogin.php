<?php
namespace User\Form;

use Doctrine\ORM\EntityManager;
use Zend\Form\Form;
use Zend\Stdlib\Hydrator\ClassMethods;
use User\Entity\UsuarioLogin as UsuarioLoginEntity;
use User\Form\Filter\UsuarioLogin as UsuarioLoginFilter;

class UsuarioLogin extends Form
{
    function __construct($name = null, EntityManager $em = null)
    {
        parent::__construct($name);

        $this->setAttribute('method', 'post')
            ->setInputFilter(new UsuarioLoginFilter())
//            ->setHydrator(new DoctrineObject($this->getObjectManager(), 'User\Entity\UsuarioLogin'));
            ->setHydrator(new ClassMethods(false))
            ->setObject(new UsuarioLoginEntity());

        $this->add(array(
            'name' => 'nome',
            'type' => 'Text',
            'options' => array(),
            'attributes' => array(
                'placeholder' => 'NOME COMPLETO',
                'class' => 'form-control',
            )
        ));
        $this->add(array(
            'name' => 'email',
            'type' => 'Email',
            'options' => array(
            ),
            'attributes' => array(
                'placeholder' => 'EMAIL',
                'class' => 'form-control',
            ),
        ));
        $this->add(array(
            'name' => 'senha',
            'type' => 'Password',
            'options' => array(),
            'attributes' => array(
                'placeholder' => 'SENHA',
                'class' => 'form-control',
            ),
        ));
        $this->add(array(
            'name' => 'confirmation',
            'type' => 'Password',
            'options' => array(),
            'attributes' => array(
                'placeholder' => 'REDIGITE SUA SENHA',
                'class' => 'form-control',
            ),
        ));

        $this->add(array(
            'name' => 'temas',
            'type' => 'DoctrineModule\Form\Element\ObjectMultiCheckbox',
            'options' => array(
                'label' => 'Escolha de 1 a 6 temas de sua preferência',
                'object_manager' => $em,
                'target_class' => 'Tema\Entity\Tema',
                'property' => 'nome',
            ),
        ));

        $this->add(array(
            'name' => 'csrf',
            'type' => 'Zend\Form\Element\Csrf',
        ));

        $this->add(array(
            'name' => 'Submit',
            'type'=>'Zend\Form\Element\Submit',
            'attributes' => array(
                'value'=>'Cadastrar',
                'class' => 'btn-success'
            )
        ));
    }
}