<?php
namespace User\Form;

use Zend\Form\Form;
use Zend\Stdlib\Hydrator\AbstractHydrator;

class Usuario extends Form
{
    protected $name;
    protected $em;
    protected $parent;
    protected $hydrator;
    protected $entity;
    protected $filter;

    public function __construct(
        $name = null,
        AbstractHydrator $hydrator,
        $entity,
        $filter = null)
    {
        parent::__construct($name);

        $this->setHydrator($hydrator)
            ->setObject($entity)
            ->setInputFilter($filter)
            ->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'cpf',
            'type' => 'Text',
            'options' => array(),
            'attributes' => array(
                'placeholder' => 'CPF',
                'class' => 'form-control',
            )
        ));
        $this->add(array(
            'name' => 'dtNascimento',
            'type' => 'Text',
            'options' => array(
//                'label' => 'Data de Nascimento'
            ),
            'attributes' => array(
                'placeholder' => 'DATA DE NASCIMENTO',
                'class' => 'form-control',
            ),
        ));
        $this->add(array(
            'name' => 'cidade',
            'type' => 'text',
            'options' => array(
            ),
            'attributes' => array(
                'placeholder' => 'CIDADE',
                'class' => 'form-control',
            ),
        ));
        $this->add(array(
            'name' => 'estado',
            'type' => 'text',
            'options' => array(
            ),
            'attributes' => array(
                'placeholder' => 'ESTADO',
                'class' => 'form-control',
            ),
        ));
        $this->add(array(
            'name' => 'telCelular',
            'type' => 'text',
            'options' => array(
            ),
            'attributes' => array(
                'placeholder' => 'TELEFONE CELULAR',
                'class' => 'form-control',
            ),
        ));
        $this->add(array(
            'name' => 'telResidencial',
            'type' => 'text',
            'options' => array(
            ),
            'attributes' => array(
                'placeholder' => 'TELEFONE RESIDENCIAL',
                'class' => 'form-control',
            ),
        ));
        $this->add(array(
            'name' => 'telComercial',
            'type' => 'text',
            'options' => array(
            ),
            'attributes' => array(
                'placeholder' => 'TELEFONE COMERCIAL',
                'class' => 'form-control',
            ),
        ));
        $this->add(array(
            'name' => 'sitePortifolio',
            'type' => 'text',
            'options' => array(
            ),
            'attributes' => array(
                'placeholder' => 'URL PORTIFOLIO',
                'class' => 'form-control',
            ),
        ));
        $this->add(array(
            'name' => 'urlFacebook',
            'type' => 'text',
            'options' => array(
            ),
            'attributes' => array(
                'placeholder' => 'URL FACEBOOK',
                'class' => 'form-control',
            ),
        ));
        $this->add(array(
            'name' => 'urlLinkedin',
            'type' => 'text',
            'options' => array(
            ),
            'attributes' => array(
                'placeholder' => 'URL LINKEDIN',
                'class' => 'form-control',
            ),
        ));
        $this->add(array(
            'name' => 'contaoSkype',
            'type' => 'text',
            'options' => array(
            ),
            'attributes' => array(
                'placeholder' => 'SKYPE',
                'class' => 'form-control',
            ),
        ));

        $this->add(array(
            'name' => 'csrf',
            'type' => 'Zend\Form\Element\Csrf',
        ));
        $this->add(array(
            'name' => 'Submit',
            'type'=>'Zend\Form\Element\Submit',
            'attributes' => array(
                'value'=>'SALVAR',
                'class' => 'btn btn-success'
            )
        ));
    }
}