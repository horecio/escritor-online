<?php
namespace User\Form\Filter;

use Zend\InputFilter\InputFilter;

class UsuarioLogin extends InputFilter{
    function __construct()
    {
        $this->add(array(
            'name' => 'nome',
            'required' => true,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
                array(
                    'name' => 'NotEmpty',
                    'options' => array(
                        'messages' => array(
                            'isEmpty' => 'Não pode estar em branco',
                        )
                    ),
                ),
//                array(
//                    'name'    => 'StringLength',
//                    'options' => array(
//                        'encoding' => 'UTF-8',
//                        'min'      => 5,
//                        'max'      => 255,
//                    ),
//                ),
            )
        ));
        $this->add(array(
            'name' => 'email',
            'required' => true,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
                array(
                    'name' => 'NotEmpty',
                    'options' => array(
                        'messages' => array(
                            'isEmpty' => 'Não pode estar em branco'
                        )
                    ),
                ),
                array(
                    'name' => 'Regex',
                    'options' => array(
                        'pattern' => '/^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/',
                        'messages' => array(
                            'regexNotMatch' => 'Seu email está estranho. Tente novamente'
                        )
                    )
                )
            )
        ));
        $this->add(array(
            'name' => 'senha',
            'required' => true,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
                array(
                    'name' => 'NotEmpty',
                    'options' => array(
                        'messages' => array('isEmpty' => 'Não pode estar em branco')
                    )
                )
            )
        ));
        $this->add(array(
            'name' => 'confirmation',
            'required' => true,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
                array('name' => 'NotEmpty',
                    'options' => array(
                        'messages' => array('isEmpty' => 'Não pode estar em branco')
                    ),
                ),
                array('name' => 'Identical',
                    'options' => array(
                        'token' => 'senha',
                        'messages' => array('notSame' => 'Acho que você redigitou a SENHA errada')
                    )
                )
            )
        ));
        $this->add(array(
            'name' => 'temas',
            'required' => true,
            'filters' => array(
            ),
            'validators' => array(
//                array('name' => 'LessThan',
//                    'options' => array(
//                        'max' => 2,
//                        'inclusive' => true
//                    )
//                )
            )
        ));

    }
}
