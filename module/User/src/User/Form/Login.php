<?php
namespace User\Form;

use Zend\Form\Form;

class Login  extends Form
{
    public function __construct($name = 'login')
    {
        parent::__construct($name);
        $this->setAttribute('method', 'post')
            ->setInputFilter(new Filter\Login());

        $this->add(array(
                'name' => 'email',
                'type' => 'Text',
                'options' => array(),
                'attributes' => array(
                    'placeholder' => 'EMAIL',
                    'class' => 'form-control',
                )
            )
        );
        $this->add(array(
            'name' => 'passwd',
            'type' => 'Password',
            'options' => array(),
            'attributes' => array(
                'placeholder' => 'SENHA',
                'class' => 'form-control',
            ),
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Csrf',
            'name' => 'csrf'
        ));
        $this->add(array(
            'name' => 'Submit',
            'type'=>'Zend\Form\Element\Submit',
            'attributes' => array(
                'value'=>'Autenticar',
                'class' => 'btn btn-success'
            )
        ));
    }
}
