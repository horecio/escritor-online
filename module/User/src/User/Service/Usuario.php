<?php
namespace User\Service;

use Base\Service\AbstractService;
use Doctrine\ORM\EntityManager;

class Usuario extends AbstractService
{
    function __construct(EntityManager $em)
    {
        parent::__construct($em);
        $this->entity = 'User\Entity\UsuarioPessoal';
    }
}