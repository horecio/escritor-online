<?php
namespace User\Service;

use Base\Service\AbstractService;
use Doctrine\ORM\EntityManager;

class UsuarioLogin extends AbstractService
{
    function __construct(EntityManager $em)
    {
        parent::__construct($em);
        $this->entity = 'User\Entity\UsuarioLogin';
    }

    public function enable($id)
    {
        if (!is_int($id)) {
            $messageException = 'Id deve ser um número inteiro';
            throw new \InvalidArgumentException($messageException);
        }

        if ($id <= 0) {
            $messageException = 'Id deve ser maior do que zero';
            throw new \OutOfBoundsException($messageException);
        }

        $entity = $this->getEm()
            ->getReference($this->entity, $id);
        $entity->setStatus(true);

        $this->getEm()->persist($entity);
        $this->getEm()->flush();

        return $entity;
    }
}