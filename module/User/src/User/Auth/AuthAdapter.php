<?php
namespace User\Auth;

use Doctrine\ORM\EntityManager;
use Zend\Authentication\Adapter\AdapterInterface;
use Zend\Authentication\Result;

class AuthAdapter implements AdapterInterface
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;

    protected $username;
    protected $passwd;
    protected $userEntity;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->userEntity = 'User\Entity\UsuarioLogin';
    }

    public function authenticate()
    {
        $repository = $this->getEm()
            ->getRepository($this->userEntity);
        $user = $repository->findByEmailAndPasswd(
            $this->getUsername(),
            $this->getPasswd()
        );
        if ($user instanceof \User\Entity\UsuarioLogin) {
            if ($user->getStatus())
                return new Result(Result::SUCCESS, array('user' => $user), array('OK'));

            return new Result(Result::SUCCESS, array('user' => $user), array('Usuário não confirmou Email'));
        }
        //usuário existe e pass incorreto
        if ($user == 'pass')
            return new Result(Result::FAILURE_CREDENTIAL_INVALID, null, array('Usuário ou Senha Inválido'));
        //usuário não encontrado
        if ($user == 'user')
            return new Result(Result::FAILURE_IDENTITY_NOT_FOUND, null, array('Usuário Não cadastrado'));
    }

    /**
     * @return \Doctrine\ORM\EntityManager
     */
    public function getEm()
    {
        return $this->em;
    }

    /**
     * @param mixed $passwd
     * @return AuthAdapter
     */
    public function setPasswd($passwd)
    {
        $this->passwd = $passwd;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPasswd()
    {
        return $this->passwd;
    }

    /**
     * @param mixed $username
     * @return AuthAdapter
     */
    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }


}