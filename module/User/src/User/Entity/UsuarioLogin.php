<?php
namespace User\Entity;

use Doctrine\ORM\Mapping as ORM;
use Zend\Stdlib\Hydrator\ClassMethods;

/**
 * UsuarioLogin
 *
 * @ORM\Table(name="eo_t_usuario_login", uniqueConstraints={@ORM\UniqueConstraint(name="id_UNIQUE", columns={"id"})})
 * @ORM\Entity(repositoryClass="User\Entity\Repository\UsuarioLogin")
 * @ORM\HaslifecycleCallbacks
 */
class UsuarioLogin
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=255, nullable=false)
     */
    private $nome;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=false)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="senha", type="string", length=255, nullable=false)
     */
    private $senha;

    /**
     * @var string
     *
     * @ORM\Column(name="salt", type="string", length=255, nullable=false)
     */
    private $salt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    private $updatedAt;

    /**
     * @var boolean
     *
     * @ORM\Column(name="status", type="boolean", nullable=false)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="activation_key", type="string", length=255, nullable=false)
     */
    private $activationKey;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="\Tema\Entity\Tema", inversedBy="usuarioLogin")
     * @ORM\JoinTable(name="eo_t_usuario_login_has_eo_t_tema",
     *   joinColumns={
     *     @ORM\JoinColumn(name="usuario_login_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="tema_id", referencedColumnName="id")
     *   }
     * )
     */
    private $tema;

    function __construct()
    {
        $this->tema = new \Doctrine\Common\Collections\ArrayCollection();
        $this->setCreatedAt();
        $this->setUpdatedAt();
        $this->setStatus(false);

        //Gera salt
        $this->setSalt(base64_encode(\Zend\Math\Rand::getBytes(16, true)));
        //Gera ActivationKey que é o email concatenado com o salt
        $this->setActivationKey(md5($this->getEmail() . $this->getSalt()));
    }

    /**
     * @return \DateTime
     */
    public function getTime()
    {
        return new \DateTime('now', new \DateTimeZone('America/Araguaina'));;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nome
     *
     * @param string $nome
     * @return EoTUsuarioLogin
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return EoTUsuarioLogin
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set senha
     *
     * @param string $senha
     * @return EoTUsuarioLogin
     */
    public function setSenha($senha)
    {
        $this->senha = $this->encryptPasswd($senha);

        return $this;
    }

    /**
     * Get senha
     *
     * @return string
     */
    public function getSenha()
    {
        return $this->senha;
    }

    /**
     * @param $senha
     * @return bool|false|string
     */
    public function encryptPasswd($senha)
    {
        $options = array(
            'salt' => $this->getSalt()
        );
        return password_hash($senha, PASSWORD_DEFAULT, $options);
    }

    /**
     * Set salt
     *
     * @param string $salt
     * @return EoTUsuarioLogin
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * Get salt
     *
     * @return string
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * Set createdAt
     *
     * @internal param \DateTime $createdAt
     * @return UsuarioLogin
     */
    public function setCreatedAt()
    {
        $this->createdAt = $this->getTime();

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @ORM\preUpdate
     * Set updatedAt
     *
     * @internal param \DateTime $updatedAt
     * @return UsuarioLogin
     */
    public function setUpdatedAt()
    {
        $this->updatedAt = $this->getTime();

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set status
     *
     * @param boolean $status
     * @return UsuarioLogin
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set activationKey
     *
     * @param string $activationKey
     * @return UsuarioLogin
     */
    public function setActivationKey($activationKey)
    {
        $this->activationKey = $activationKey;

        return $this;
    }

    /**
     * Get activationKey
     *
     * @return string
     */
    public function getActivationKey()
    {
        return $this->activationKey;
    }

    /**
     * Add tema
     *
     * @param \Tema\Entity\Tema $tema
     * @return UsuarioLogin
     */
    public function addTema(\Tema\Entity\Tema $tema)
    {
        $this->tema[] = $tema;

        return $this;
    }


    /**
     * Remove tema
     *
     * @param \Tema\Entity\Tema $tema
     */
    public function removeTema(\Tema\Entity\Tema $tema)
    {
        $this->tema->removeElement($tema);
    }

    /**
     * Get tema
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTema()
    {
        return $this->tema;
    }

    public function toArray()
    {
        $hydrator = new ClassMethods();

        return $hydrator->extract($this);
    }
}
