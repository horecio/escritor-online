<?php
namespace User\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Usuario
 *
 * @ORM\Table(name="eo_t_usuario", uniqueConstraints={@ORM\UniqueConstraint(name="id_UNIQUE", columns={"id"})}, indexes={@ORM\Index(name="fk_eo_t_usuario_eo_t_usuario_login1_idx", columns={"eo_t_usuario_login_id"})})
 * @ORM\Entity
 */
class Usuario
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="cpf", type="string", length=14, nullable=false)
     */
    private $cpf;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_nascimento", type="date", nullable=false)
     */
    private $dtNascimento;

    /**
     * @var string
     *
     * @ORM\Column(name="cidade", type="string", length=255, nullable=false)
     */
    private $cidade;

    /**
     * @var string
     *
     * @ORM\Column(name="estado", type="string", length=255, nullable=false)
     */
    private $estado;

    /**
     * @var string
     *
     * @ORM\Column(name="tel_residencial", type="string", length=30, nullable=true)
     */
    private $telResidencial;

    /**
     * @var string
     *
     * @ORM\Column(name="tel_celular", type="string", length=30, nullable=true)
     */
    private $telCelular;

    /**
     * @var string
     *
     * @ORM\Column(name="tel_comercial", type="string", length=30, nullable=true)
     */
    private $telComercial;

    /**
     * @var string
     *
     * @ORM\Column(name="site_portifolio", type="string", length=255, nullable=true)
     */
    private $sitePortifolio;

    /**
     * @var string
     *
     * @ORM\Column(name="url_facebook", type="string", length=255, nullable=true)
     */
    private $urlFacebook;

    /**
     * @var string
     *
     * @ORM\Column(name="url_linkedin", type="string", length=45, nullable=true)
     */
    private $urlLinkedin;

    /**
     * @var string
     *
     * @ORM\Column(name="contato_skype", type="string", length=255, nullable=true)
     */
    private $contatoSkype;

    /**
     * @var \User\Entity\UsuarioLogin
     *
     * @ORM\ManyToOne(targetEntity="User\Entity\UsuarioLogin")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="eo_t_usuario_login_id", referencedColumnName="id")
     * })
     */
    private $usuarioLogin;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cpf
     *
     * @param string $cpf
     * @return Usuario
     */
    public function setCpf($cpf)
    {
        $this->cpf = $cpf;

        return $this;
    }

    /**
     * Get cpf
     *
     * @return string 
     */
    public function getCpf()
    {
        return $this->cpf;
    }

    /**
     * Set dtNascimento
     *
     * @param \DateTime $dtNascimento
     * @return Usuario
     */
    public function setDtNascimento(\DateTime $dtNascimento)
    {
        $this->dtNascimento = $dtNascimento;

        return $this;
    }

    /**
     * Get dtNascimento
     *
     * @return \DateTime 
     */
    public function getDtNascimento()
    {
        return $this->dtNascimento;
    }

    /**
     * Set cidade
     *
     * @param string $cidade
     * @return Usuario
     */
    public function setCidade($cidade)
    {
        $this->cidade = $cidade;

        return $this;
    }

    /**
     * Get cidade
     *
     * @return string 
     */
    public function getCidade()
    {
        return $this->cidade;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return Usuario
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set telResidencial
     *
     * @param string $telResidencial
     * @return Usuario
     */
    public function setTelResidencial($telResidencial)
    {
        $this->telResidencial = $telResidencial;

        return $this;
    }

    /**
     * Get telResidencial
     *
     * @return string 
     */
    public function getTelResidencial()
    {
        return $this->telResidencial;
    }

    /**
     * Set telCelular
     *
     * @param string $telCelular
     * @return Usuario
     */
    public function setTelCelular($telCelular)
    {
        $this->telCelular = $telCelular;

        return $this;
    }

    /**
     * Get telCelular
     *
     * @return string 
     */
    public function getTelCelular()
    {
        return $this->telCelular;
    }

    /**
     * Set telComercial
     *
     * @param string $telComercial
     * @return Usuario
     */
    public function setTelComercial($telComercial)
    {
        $this->telComercial = $telComercial;

        return $this;
    }

    /**
     * Get telComercial
     *
     * @return string 
     */
    public function getTelComercial()
    {
        return $this->telComercial;
    }

    /**
     * Set sitePortifolio
     *
     * @param string $sitePortifolio
     * @return Usuario
     */
    public function setSitePortifolio($sitePortifolio)
    {
        $this->sitePortifolio = $sitePortifolio;

        return $this;
    }

    /**
     * Get sitePortifolio
     *
     * @return string 
     */
    public function getSitePortifolio()
    {
        return $this->sitePortifolio;
    }

    /**
     * Set urlFacebook
     *
     * @param string $urlFacebook
     * @return Usuario
     */
    public function setUrlFacebook($urlFacebook)
    {
        $this->urlFacebook = $urlFacebook;

        return $this;
    }

    /**
     * Get urlFacebook
     *
     * @return string 
     */
    public function getUrlFacebook()
    {
        return $this->urlFacebook;
    }

    /**
     * Set urlLinkedin
     *
     * @param string $urlLinkedin
     * @return Usuario
     */
    public function setUrlLinkedin($urlLinkedin)
    {
        $this->urlLinkedin = $urlLinkedin;

        return $this;
    }

    /**
     * Get urlLinkedin
     *
     * @return string 
     */
    public function getUrlLinkedin()
    {
        return $this->urlLinkedin;
    }

    /**
     * Set contatoSkype
     *
     * @param string $contatoSkype
     * @return Usuario
     */
    public function setContatoSkype($contatoSkype)
    {
        $this->contatoSkype = $contatoSkype;

        return $this;
    }

    /**
     * Get contatoSkype
     *
     * @return string 
     */
    public function getContatoSkype()
    {
        return $this->contatoSkype;
    }

    /**
     * Set eoTUsuarioLogin
     *
     * @param \User\Entity\UsuarioLogin $usuarioLogin
     * @return Usuario
     */
    public function setUsuarioLogin(\User\Entity\UsuarioLogin $usuarioLogin = null)
    {
        $this->usuarioLogin = $usuarioLogin;

        return $this;
    }

    /**
     * Get UsuarioLogin
     *
     * @return \User\Entity\UsuarioLogin
     */
    public function getUsuarioLogin()
    {
        return $this->usuarioLogin;
    }
}
