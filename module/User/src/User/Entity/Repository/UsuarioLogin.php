<?php
namespace User\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class UsuarioLogin extends EntityRepository
{
    public function findByEmailAndPasswd($email, $passwd)
    {
        $user = $this->findOneByEmail($email);
        if($user) {
            $hashSenha = $user->encryptPasswd($passwd);

            if($hashSenha == $user->getSenha())
                return $user;

            //senha não confere
            return 'pass';
        }
        //usuário não existe
        return 'user';

    }
} 