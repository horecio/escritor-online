<?php
namespace User\Controller;

use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Storage\Session;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class AuthController extends AbstractActionController
{
    //Auth
    protected $authAdapter;
    protected $controller;
    protected $form;
    protected $route;
    protected $nameSession; //nome da Sessão

    //User
    protected $userController;
    protected $userRoute;

    //Perfil
    protected $perfilController;
    protected $perfilRoute;
    protected $perfilAction;

    function __construct()
    {
        //Auth
        $this->authAdapter = 'User\Auth\AuthAdapter';
        $this->controller = 'auth';
        $this->form = 'User\Form\Login';
        $this->route = 'user-auth';
        $this->nameSession = 'User';

        //User
        $this->userController = 'index';
        $this->userRoute = 'user/default';

        //Perfil
        $this->perfilController = 'perfil';
        $this->perfilAction = 'index';
        $this->perfilRoute = 'user-perfil';
    }

    public function indexAction()
    {
        $form = $this->getServiceLocator()
            ->get($this->form);
        $error = false;

        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $data = $form->getData();

                //Cria storage para gravar sessão de autenticação
                $sessionStorage = new Session($this->nameSession);

                $auth = new AuthenticationService();
                $auth->setStorage($sessionStorage); //define a SessionStorage para Auth

                //Seta login e password no AuthAdapter
                $authAdapter = $this->getServiceLocator()
                    ->get('User\Auth\AuthAdapter');
                $authAdapter->setUserName($data['email'])
                    ->setPasswd($data['passwd']);

                //faz Autenticação
                $result = $auth->authenticate($authAdapter);

                //Se login e senha estiverem corretos
                if ($result->isValid()) {
                    //Verifica se status é false
                    if (!$result->getIdentity()['user']->getStatus()) {
                        $auth->clearIdentity();
                        $this->flashMessenger()->addErrorMessage("Usuário não está ativo. Verifique seu Email.");
                        return $this->redirect()
                            ->toRoute($this->route);
                    }
                    $sessionStorage->write($auth->getIdentity(), null);

                    //Primeiro Nome do User logado
                    $nome = explode(' ', $result->getIdentity()['user']->getNome())[0];
                    return $this->redirect()
                        ->toRoute($this->perfilRoute,
                            array('name' => $nome)
                        );
                }
                if ($result->getCode() == -3){
                    $this->flashMessenger()->addErrorMessage("Login ou senha errados.");
                    return $this->redirect()
                        ->toRoute($this->route);
                }
                //User não existe
                if ($result->getCode() == -1) {
                    $route = 'user/index/add';
                    $this->flashMessenger()
                        ->addErrorMessage(
                            'USuário não cadastrado. Quer se Cadastrar?. <a href="' . $route . '">Clique aqui</a>'
                        );
                    return $this->redirect()
                        ->toRoute($this->route);
                }
            }
        }

        return new ViewModel(array(
            'form' => $form,
            'error' => $error,
            'controller' => $this->controller
        ));
    }

    public function errorAction()
    {

    }

    public function logoutAction()
    {
        $auth = new AuthenticationService;
        $auth->setStorage(new Session($this->nameSession));
        $auth->clearIdentity();

        //Redireciona para página de login
        return $this->redirect()->toRoute($this->route);
    }
} 