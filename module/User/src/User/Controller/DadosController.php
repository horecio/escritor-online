<?php
namespace User\Controller;

use Base\Controller\AbstractController;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Storage\Session;
use Zend\View\Model\ViewModel;

class DadosController extends AbstractController
{
    protected $entityLogin;

    function __construct()
    {
        $this->entity = 'User\Entity\Usuario';
        $this->form = 'User\Form\Usuario';
        $this->service = 'User\Service\Usuario';
        $this->controller = 'dados';
        $this->route = 'user/default';

        //Login
        $this->entityLogin = 'User\Entity\UsuarioLogin';
    }

    public function addAction()
    {
        $auth = new AuthenticationService;
        $auth->setStorage(new Session('User'));

        $form = $this->getServiceLocator()
            ->get($this->form);
        $request = $this->getRequest();

        if ($request->isPost()) {
            //Só consegue salvar se estiver Logado
            if ($auth->hasIdentity()) {
                $user = $auth->getIdentity()['user'];
                $id = $user->getId();
                $entityLogin = $this->getEm()
                    ->getReference($this->entityLogin, $id);

                $entity = $this->getServiceLocator()
                    ->get($this->entity);
                $entity->setUsuarioLogin($entityLogin);
                $form->bind($entity);

            }
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $service = $this->getServiceLocator()->get($this->service);
                try {
                    $service->insert($form->getData());

                    $this->flashMessenger()->addSuccessMessage("Cadastrado com sucesso");
                    return $this->redirect()->toRoute($this->route, array('controller' => 'perfil'));
                } catch (\Exception $e) {
                    $this->flashMessenger()->addErrorMessage("Ops! Algum problema ocorreu ao inserir o usuário");
                    return $this->redirect()
                        ->toRoute($this->route, array('controller' => 'index'));
                }
            }
        }
        return new ViewModel(array(
            'form' => $form,
            'controller' => $this->controller,
            'route' => $this->route
        ));
    }
} 