<?php
namespace User\Controller;

use Base\Controller\AbstractController;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Storage\Session;
use Zend\View\Model\ViewModel;

class PerfilController extends AbstractController
{
    function __construct()
    {
        $this->controller = 'perfil';
        $this->action = 'index';
        $this->route = 'user-perfil';
    }

    public function indexAction()
    {
        $auth = new AuthenticationService;
        $auth->setStorage(new Session('User'));

        $userDados = null;
        $projetos = null;
        $user = null;
        $temas = 'Nenhum Tema';
        if ($auth->hasIdentity()) {
            $user = $auth->getIdentity()['user']->toArray();
            unset($user['passwd']);
            unset($user['salt']);
            unset($user['activation_key']);
            $id = $user['id'];

            //Busca usuário Logado
            $usuario = $this->getEm()
                ->getRepository('User\Entity\UsuarioLogin')
                ->find($id);

            $userDados = $this->getEm()
                ->getRepository('User\Entity\Usuario')
                ->findOneByUsuarioLogin($id);

            $projetos = $this->getEm()
                ->getRepository('Projeto\Entity\Projeto')
                ->findBy(
                    array('usuarioLogin' => $id)
                );

            //Get Temas preferido do usuário
            $temas = $usuario->getTema()
                ->getValues();
        }
        return new ViewModel(array(
            'user' => $user,
            'userDados' => $userDados,
            'temas' => $temas,
            'projetos' => $projetos,
            'route' => $this->route,
            'controller' => $this->controller
        ));

    }

} 