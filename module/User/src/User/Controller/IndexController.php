<?php
namespace User\Controller;

use Base\Controller\AbstractController;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractController
{
    protected $entityUsuario;
    protected $controllerUsuario;

    function __construct()
    {
        //Login
        $this->entity = 'User\Entity\UsuarioLogin';
        $this->form = 'User\Form\UsuarioLogin';
        $this->service = 'User\Service\UsuarioLogin';
        $this->controller = 'index';
        $this->action = 'index';
        $this->route = 'user/default';
        //Usuario
        $this->entityUsuario = 'User\Entity\Usuario';
        $this->controllerUsuario = 'dados';
    }

    public function addAction()
    {
        $form = $this->getServiceLocator()
            ->get($this->form);
        $request = $this->getRequest();

        if ($request->isPost()) {
            $entity = $this->getServiceLocator()
                ->get($this->entity);

            //Alterar para Strategie
            //Adiciona as entities de Tema ao ArrayCollection na Entity de UsuarioLogin
            if (isset($request->getPost()['temas']))
                $entityTema = $this->populateEntity($entity, $request->getPost()['temas']);

            $form->bind($entity);

            $form->setData($request->getPost());
            if ($form->isValid()) {
                $service = $this->getServiceLocator()->get($this->service);
                try {
                    $service->insert($form->getData());

                    $this->flashMessenger()->addSuccessMessage("Cadastrado com sucesso");
                    return $this->redirect()->toRoute($this->route);
                } catch (\Exception $e) {
                    $this->flashMessenger()->addErrorMessage("Ops! Algum problema ocorreu ao inserir o usuário" . $e->getMessage());
                    return $this->redirect()
                        ->toRoute($this->route);
                }
            }
        }
        return new ViewModel(array(
            'form' => $form,
            'controller' => $this->controller,
            'route' => $this->route
        ));
    }

    public function enableAction()
    {
        if (!$id = (int)$this->params()->fromRoute('id', 0)) {
            return $this->redirect()
                ->toRoute($this->route, array('controller' => $this->controller));
        }

        $service = $this->getServiceLocator()
            ->get($this->service);

        try {
            $service->enable($id);
            $this->flashMessenger()->addSuccessMessage('Ativado com sucesso');
            return $this->redirect()
                ->toRoute(
                    $this->route,
                    array('controller' => $this->controller));
        } catch (\Exception $e) {
            $this->flashMessenger()->addErrorMessage('Parece que esse ID não existe');
            return $this->redirect()
                ->toRoute($this->route,
                    array('controller' => $this->controller));
        }
    }

    public function detailAction()
    {
        if (!$id = (int)$this->params()->fromRoute('id', 0)) {
            $this->flashMessenger()->addErrorMessage("Parece que esse ID não existe.");
            return $this->redirect()
                ->toRoute($this->route);
        }
        try {
            $entity = $this->getEm()
                ->getRepository('User\Entity\Usuario')
                ->findOneByUsuarioLogin($id);

        } catch (\Exception $e) {
            $this->flashMessenger()->addErrorMessage("Ops! Algum problema ocorreu ao mostrar detalhes" . $e->getMessage());
            return $this->redirect()
                ->toRoute($this->route,
                    array('controller' => $this->controller));
        }

        return new ViewModel(array(
            'controller' => $this->controllerUsuario,
            'detail' => $entity,
            'route' => $this->route
        ));
    }

    public function populateEntity($entity, array $data)
    {
        foreach ($data as $dt) {
            $entityTema = $this->getEm()
                ->getReference('Tema\Entity\Tema', $dt);

            $entity->addTema($entityTema);
        }
        return $entity;
    }
}