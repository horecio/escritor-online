<?php
namespace User;

use Base\Hydrator\Strategy\DateTimeStrategy;
use Zend\Stdlib\Hydrator\ClassMethods;

class Module
{
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                //UsuarioLogin
                'User\Service\UsuarioLogin' => function($serviceManager) {
                        return new Service\UsuarioLogin(
                            $serviceManager->get('Doctrine\ORM\EntityManager')
                        );
                    },
                'User\Form\UsuarioLogin' => function($serviceManager) {
                        return new Form\UsuarioLogin(
                            'usuario-login',
                            $serviceManager->get('Doctrine\ORM\EntityManager')
                        );
                    },
                //Auth
                'User\Auth\AuthAdapter' => function ($serviceManager) {
                        return new Auth\AuthAdapter(
                            $serviceManager->get('Doctrine\ORM\EntityManager')
                        );
                    },
                //Usuario
                'User\Service\Usuario' => function($serviceManager) {
                        return new Service\Usuario(
                            $serviceManager->get('Doctrine\ORM\EntityManager')
                        );
                    },
                'User\Form\Usuario' => function ($serviceManager) {
                        $hydrator = new ClassMethods(false);
                        $hydrator->addStrategy('dtNascimento', new DateTimeStrategy());

                        return new Form\Usuario(
                            'usuario',
                            $hydrator,
                            $serviceManager->get('User\Entity\Usuario'),
                            $serviceManager->get('User\Form\Filter\Usuario')
                        );
                    },
            ),
            'invokables' => array(
                //UsuarioLogin
                'User\Entity\UsuarioLogin' => 'User\Entity\UsuarioLogin',
                //Auth
                'User\Form\Login' => 'User\Form\Login',
                //UsuarioPessoal
                'User\Entity\Usuario' => 'User\Entity\Usuario',
                'User\Form\Filter\Usuario' => 'User\Form\Filter\Usuario',
            )
        );
    }
}